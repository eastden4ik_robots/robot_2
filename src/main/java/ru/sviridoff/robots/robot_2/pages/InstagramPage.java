package ru.sviridoff.robots.robot_2.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import ru.sviridoff.framework.driver.SeleniumUtils;
import ru.sviridoff.framework.logger.Logger;
import ru.sviridoff.framework.page_object_pattern.AbstractPageObject;
import ru.sviridoff.framework.xpath_definitions.XpathUtils;


public class InstagramPage extends AbstractPageObject {


    private XpathUtils xpathUtils;
    private SeleniumUtils seleniumUtils = new SeleniumUtils();

    private Logger log = new Logger(InstagramPage.class.getName() + " - " + getPageName() + " - " + getWebDriver().getTitle());

    InstagramPage(WebDriver driver) {
        super("Главная страница", driver);
        xpathUtils = new XpathUtils(getXpathsModelList(), getPageName());
    }

    public InstagramPage isExists() {
        String page = xpathUtils.getXpathByType("Page");
        if (seleniumUtils.isVisible(getWebDriver(), By.xpath(page), 30)) {
            log.success("Страница [" + getPageName() + "] отобразилась корректно.");
            return this;
        } else {
            log.error("Страница [" + getPageName() + "] не отобразилась.");
            throw new RuntimeException("Страница [" + getPageName() + "] не отобразилась.");
        }
    }

    public void searchByTagName(String tagName) {
        String search = xpathUtils.getXpathByName("Поиск");
        String searchDialog = xpathUtils.getXpathByName("Диалог поиска");
        String firstLink = xpathUtils.getXpathByName("Первая ссылка");
        String photo = xpathUtils.getXpathByName("Фото");
        String like = xpathUtils.getXpathByName("Like");
        if (seleniumUtils.isVisible(getWebDriver(), By.xpath(search), 30)) {
            getWebDriver().findElement(By.xpath(search)).clear();
            getWebDriver().findElement(By.xpath(search)).sendKeys(tagName + Keys.ENTER);
            if (seleniumUtils.isVisible(getWebDriver(), By.xpath(searchDialog), 30)) {
                getWebDriver().findElement(By.xpath(firstLink)).click();

                if (seleniumUtils.isVisible(getWebDriver(), By.xpath(photo), 90)) {

                    String href = getWebDriver().findElement(By.xpath(photo)).getAttribute("href");
                    log.success(href);
                    getWebDriver().get(href);
                    if (seleniumUtils.isVisible(getWebDriver(), By.xpath(like), 90)) {

                        getWebDriver().findElement(By.xpath(like)).click();

                    }
                }
            }
        }
    }

    public void subscribeByName(String name) {
        searchByTagName(name);
        if (seleniumUtils.isVisible(getWebDriver(), By.xpath(xpathUtils.getXpathByName("Подписаться")), 30)) {
            getWebDriver().findElement(By.xpath(xpathUtils.getXpathByName("Подписаться"))).click();
        }
    }

    public void unSubscribeByName(String name) {
        searchByTagName(name);
        if (seleniumUtils.isVisible(getWebDriver(), By.xpath(xpathUtils.getXpathByName("Подписка")), 30)) {
            if (seleniumUtils.isVisible(getWebDriver(), By.xpath(xpathUtils.getXpathByName("Отписаться")), 30)) {
                getWebDriver().findElement(By.xpath(xpathUtils.getXpathByName("Отписаться"))).click();
            }
        }
    }

}
