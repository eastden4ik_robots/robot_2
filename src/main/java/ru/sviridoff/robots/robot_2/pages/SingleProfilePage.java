package ru.sviridoff.robots.robot_2.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import ru.sviridoff.framework.driver.SeleniumUtils;
import ru.sviridoff.framework.logger.Logger;
import ru.sviridoff.framework.page_object_pattern.AbstractPageObject;
import ru.sviridoff.framework.xpath_definitions.XpathUtils;

public class SingleProfilePage extends AbstractPageObject {

    private XpathUtils xpathUtils;
    private SeleniumUtils seleniumUtils = new SeleniumUtils();

    private Logger log = new Logger(SingleProfilePage.class.getName() + " - " + getPageName() + " - " + getWebDriver().getTitle());

    public SingleProfilePage(WebDriver driver) {
        super("Страница профиля", driver);
        xpathUtils = new XpathUtils(getXpathsModelList(), getPageName());
    }

    public SingleProfilePage isExists() {
        String page = xpathUtils.getXpathByType("Page");
        if (seleniumUtils.isVisible(getWebDriver(), By.xpath(page), 30)) {
            log.success("Страница [" + getPageName() + "] отобразилась корректно.");
            return this;
        } else {
            log.error("Страница [" + getPageName() + "] не отобразилась.");
            throw new RuntimeException("Страница [" + getPageName() + "] не отобразилась.");
        }
    }

}
