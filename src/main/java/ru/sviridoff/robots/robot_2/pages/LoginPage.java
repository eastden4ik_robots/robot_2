package ru.sviridoff.robots.robot_2.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import ru.sviridoff.framework.driver.SeleniumUtils;
import ru.sviridoff.framework.logger.Logger;
import ru.sviridoff.framework.page_object_pattern.AbstractPageObject;
import ru.sviridoff.framework.xpath_definitions.XpathUtils;

public class LoginPage extends AbstractPageObject {

    private XpathUtils xpathUtils;
    private SeleniumUtils seleniumUtils = new SeleniumUtils();

    private Logger log = new Logger(LoginPage.class.getName() + " - " + getPageName());

    public LoginPage(WebDriver driver) {
        super("Страница входа", driver);
        xpathUtils = new XpathUtils(getXpathsModelList(), getPageName());
    }

    public LoginPage goToLoginPage(String webSite) {
        getWebDriver().get(webSite);
        return this;
    }

    public LoginPage isExists() {
        String page = xpathUtils.getXpathByType("Page");
        if (seleniumUtils.isVisible(getWebDriver(), By.xpath(page), 30)) {
            log.success("Страница [" + getPageName() + "] отобразилась корректно.");
            return this;
        } else {
            log.error("Страница [" + getPageName() + "] не отобразилась.");
            throw new RuntimeException("Страница [" + getPageName() + "] не отобразилась.");
        }
    }

    public InstagramPage authorisationComplete() {
        log.success("Авторизация прошла успешно.");
        return new InstagramPage(getWebDriver());
    }

    public LoginPage checkNotificationsModal() {
        if (seleniumUtils.isVisible(getWebDriver(), By.xpath(xpathUtils.getXpathByName("Уведомление")), 30)) {
            getWebDriver().findElement(By.xpath(xpathUtils.getXpathByName("Не сейчас."))).click();
            log.success("Присутствовало модальное окно с уведомлением об уведомлениях и нажата кнопка Не сейчас.");
        }
        return this;
    }

    public LoginPage checkSavingCredentials() {
        if (seleniumUtils.isVisible(getWebDriver(), By.xpath(xpathUtils.getXpathByName("Сохранить данные?")), 30)) {
            getWebDriver().findElement(By.xpath(xpathUtils.getXpathByName("Не сейчас"))).click();
            log.success("Присутствовало модальное окно с уведомлением о сохранении введенных данных и нажата кнопка Не сейчас.");
        }
        return this;
    }

    public LoginPage enterCredentials(String login, String password) {
        this.insertLogin(login);
        this.insertPassword(password);
        this.clickButton();
        log.success("Введены контактные данные и нажата кнопка войти.");
        return this;
    }

    private void clickButton() {
        seleniumUtils.isPresence(getWebDriver(), By.xpath(xpathUtils.getXpathByName("Войти")), 30);
        getWebDriver().findElement(By.xpath(xpathUtils.getXpathByName("Войти"))).click();
    }

    private void insertPassword(String password) {
        seleniumUtils.isPresence(getWebDriver(), By.xpath(xpathUtils.getXpathByName("Пароль")), 30);
        getWebDriver().findElement(By.xpath(xpathUtils.getXpathByName("Пароль"))).sendKeys(password);
    }

    private void insertLogin(String login) {
        seleniumUtils.isPresence(getWebDriver(), By.xpath(xpathUtils.getXpathByName("Логин")), 30);
        getWebDriver().findElement(By.xpath(xpathUtils.getXpathByName("Логин"))).sendKeys(login);
    }

}
