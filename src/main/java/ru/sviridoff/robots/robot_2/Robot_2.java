package ru.sviridoff.robots.robot_2;

import org.openqa.selenium.WebDriver;
import ru.sviridoff.framework.driver.SeleniumDriver;
import ru.sviridoff.framework.logger.Logger;
import ru.sviridoff.framework.props.Props;
import ru.sviridoff.robots.robot_2.pages.InstagramPage;
import ru.sviridoff.robots.robot_2.pages.LoginPage;

public class Robot_2 {


    private static Logger logger = new Logger("Робот 2 - Инстаграмус.");
    private static SeleniumDriver seleniumDriver = new SeleniumDriver();

    private static WebDriver webDriver;

    private static String LOGIN;
    private static String PASSWORD;
    private static String WEB_SITE;

    private static void setUp() {

        Props props = new Props("Robot_2.properties");
        System.setProperty("xpaths.file", props.getProperty("definitions"));
        webDriver = seleniumDriver.Init(SeleniumDriver.Drivers.CHROME_WINDOWS);

        LOGIN = props.getProperty("instagram.login");
        PASSWORD = props.getProperty("instagram.pwd");
        WEB_SITE = props.getProperty("instagram.url");

    }

    public static void main(String[] args) {

        setUp();

        logger.info("Бот запущен.");

        InstagramPage instagramPage = new LoginPage(webDriver)
                .goToLoginPage(WEB_SITE)
                .enterCredentials(LOGIN, PASSWORD)
                .isExists()
                .checkSavingCredentials()
                .checkNotificationsModal()
                .authorisationComplete()
                .isExists();


        instagramPage.isExists()
                .searchByTagName("#photo");

        seleniumDriver.Destroy(webDriver);
        logger.info("Бот закончил свою работу.");

    }

}
